import { Component, OnInit } from '@angular/core';
import { VinosService } from './vinos.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  articulos:any;

  art={
    codigo:0,
    descripcion:"",
    precio:0
  }
  constructor(private VinosService: VinosService) {};
  ngOnInit() {
    this.recuperarVinos();
  }

  alta() {
    this.VinosService.alta(this.art).subscribe((datos:any) => {
      if (datos['resultado']=='OK') {
        alert(datos['mensaje']);
        this.recuperarVinos();
      }
    });
  }

  baja(codigo:number) {
    this.VinosService.baja(codigo).subscribe((datos:any) => {
      if (datos['resultado']=='OK') {
        alert(datos['mensaje']);
        this.recuperarVinos();
      }
    });
  }

  modificacion() {
    this.VinosService.modificacion(this.art).subscribe((datos:any) => {
      if (datos['resultado']=='OK') {
        alert(datos['mensaje']);
        this.recuperarVinos();
      }
    });
  }

  seleccionar(codigo:number) {
    this.VinosService.seleccionar(codigo).subscribe((result:any) => this.art = result[0]);
  }

  recuperarVinos() {
    this.VinosService.recuperarVinos().subscribe((result:any) => this.articulos = result);
  }

  hayVinos() {
    return true;
  }
}
